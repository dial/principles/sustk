import ExecutionEnvironment from '@docusaurus/ExecutionEnvironment'
import { useLocation } from '@docusaurus/router'
import { CANVAS_BLOCKS } from './constants'

/* eslint-disable import/prefer-default-export */
export const isBrowserRuntime = () => typeof window !== 'undefined'

export const getBlockNameBySlug = (slug) => {
	const canvasBlock = CANVAS_BLOCKS.filter((block) => block.slug === slug)

	return canvasBlock && canvasBlock[0] && canvasBlock[0].displayName
}

export const getLastInArray = (arr) => arr && arr.length && arr[arr.length - 1]

export const createSticky = (body, segment, order) => ({
	body,
	segment,
	order,
})

/**
 * This will take an async function and make it "patient"
 * Which is just to say, if it is called more than once it won't run
 * the second call until after the first one completes.
 *
 * Note that this implementation will ONLY call the most recent call
 * (so if you call 10 times while the first is running, only the last
 * will run).
 *
 * This was created to allow for safe storage to our API.
 *
 * I don't really know why this isn't more common.
 */
export const makePatient = (asyncFunction) => {
	let nextCall = null
	let isBusy = false
	const patientFunction = async (...args) => {
		if (!isBusy) {
			isBusy = true
			await asyncFunction(...args)
			isBusy = false
			if (nextCall) {
				patientFunction(...nextCall)
				nextCall = null
			}
		} else {
			nextCall = args
		}
	}

	return patientFunction
}

export const loadItemFromApiStorage = async (key) => {
	const response = await fetch(`/api/items/${key}`, {
		method: 'GET',
	})
	if (response.status !== 200) {
		return null
	}

	return response.json()
}

export const saveItemToApiStorage = async (key, value) => (
	fetch(`/api/items/${key}`, {
		method: 'POST',
		headers: {
			'Content-Type': 'application/json',
		},
		body: JSON.stringify({ value }),
	})
)

export function getIndexOfKeyedObject(keyedObject, keyedObjects) {
	return keyedObjects.findIndex((item) => keyedObject.key === item.key)
}

export function replaceKeyedObject(keyedObject, keyedObjects) {
	const updateIndex = getIndexOfKeyedObject(keyedObject, keyedObjects)
	if (updateIndex === -1) {
		return keyedObjects
	}

	const newKeyedObjects = [...keyedObjects]
	newKeyedObjects[updateIndex] = keyedObject

	return newKeyedObjects
}

export function generateGridOfValue(width, height, value) {
	return Array.from(
		{ length: height },
		() => Array.from(
			{ length: width },
			() => value,
		),
	)
}

export function getParamFromUrl(param) {
	if (!ExecutionEnvironment.canUseDOM) {
		return null
	}

	const { search } = useLocation()
	const value = (new URLSearchParams(search).get(param)) || null

	return value
}
