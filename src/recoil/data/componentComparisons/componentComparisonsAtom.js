import { atom } from 'recoil'
import localStorageEffect from '../../effects/localStorageEffect'

const ATOM_KEY = 'componentComparisons'

const componentComparisonsAtom = atom({
	key: ATOM_KEY,
	default: {},
	effects_UNSTABLE: [
		localStorageEffect(ATOM_KEY),
	],
})

export default componentComparisonsAtom
