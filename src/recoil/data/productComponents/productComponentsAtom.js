import { atom } from 'recoil'
import localStorageEffect from '../../effects/localStorageEffect'
import {
	DEFAULT_PRODUCT_COMPONENT_COUNT,
	DEFAULT_WRAPAROUND_PRODUCT_COMPONENTS,
	DEFAULT_NOT_PRESENT_PRODUCT_COMPONENTS,
	PRODUCT_COMPONENT_ADAPTABILITY_TYPE,
} from '../../../constants'
import { generateProductComponents } from '../../../components/tools/CoreModularHackable/utils'

const ATOM_KEY = 'productComponents'

function generateDefaultProductComponents() {
	const baseProductComponents = generateProductComponents(
		DEFAULT_PRODUCT_COMPONENT_COUNT,
		1,
	)
	const wraparoundProductComponents = generateProductComponents(
		DEFAULT_WRAPAROUND_PRODUCT_COMPONENTS,
		1 + DEFAULT_PRODUCT_COMPONENT_COUNT,
	).map((productComponent) => ({
		...productComponent,
		now: PRODUCT_COMPONENT_ADAPTABILITY_TYPE.WRAPAROUND,
	}))
	const notPresentProductComponents = generateProductComponents(
		DEFAULT_NOT_PRESENT_PRODUCT_COMPONENTS,
		1 + DEFAULT_PRODUCT_COMPONENT_COUNT + DEFAULT_WRAPAROUND_PRODUCT_COMPONENTS,
	).map((productComponent) => ({
		...productComponent,
		now: PRODUCT_COMPONENT_ADAPTABILITY_TYPE.NOT_PRESENT,
	}))

	return [
		...baseProductComponents,
		...wraparoundProductComponents,
		...notPresentProductComponents,
	]
}

const productComponentsAtom = atom({
	key: ATOM_KEY,
	default: generateDefaultProductComponents(),
	effects_UNSTABLE: [
		localStorageEffect(ATOM_KEY),
	],
})

export default productComponentsAtom
