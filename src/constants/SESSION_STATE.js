/**
 * LOGGED_OUT: Default state for a new user / session
 * LOGGING_IN: The user has authenticated and the system has to load state from the server.
 * LOGGED_IN: The user is authenticated and the system has loaded state from the server.
 * LOGGING_OUT: The user has deauthenticated and the system has to reset local data.
 * DISCONNECTED: There is a network issue.
 * RECONNECTED: There was a network issue and it has resolved.
 */

export const SESSION_STATE = {
	LOGGED_OUT: 'loggedOut',
	LOGGING_IN: 'loggingIn',
	LOGGED_IN: 'loggedIn',
	LOGGING_OUT: 'loggingOut',
	DISCONNECTED: 'disconnected',
	RECONNECTED: 'reconnected',
}
