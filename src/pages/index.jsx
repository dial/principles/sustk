import React from 'react'
import Layout from '@theme/Layout'
import Link from '@docusaurus/Link'
import CalloutUrl from '@site/static/img/callout.png'
import HeroBmcExampleUrl from '@site/static/img/hero-bmc-example.png'
import styles from './index.module.css'

export default function Home() {
	return (
		<Layout
			title="Welcome"
			description="Guidance, tools, and case studies to help digital product/service owners and advisors in the aid sector design sustainable business models. Powered by Build for Sustainability – one of the Principles for Digital Development."
		>
			<header className={`
				hero
				hero--dark
				has-canvas-background
				${styles.hero}
			`}
			>
				<div className={styles.container}>
					<div className={styles.blurb}>
						<h1 className="hero__title">
							Business Model
							{' '}
							<em>Sustainability</em>
							{' '}
							Toolkit
						</h1>
						<p className="hero__subtitle margin-vert--lg">
							Guidance, tools, and case studies to help digital product/service owners
							and advisors in the aid sector design sustainable business models.
							Powered by
							{' '}
							<Link
								to="https://digitalprinciples.org/principle/build-for-sustainability/"
								className={styles.textLink}
							>
								Build for Sustainability
							</Link>
							{' '}
							– one of the
							{' '}
							<Link
								to="https://digitalprinciples.org/"
								className={styles.textLink}
							>
								Principles for Digital Development
							</Link>
							.
						</p>
						<div className={styles.buttons}>
							<Link
								to="/guide"
								className="button button--primary button--inverted"
							>
								Read the Guide
							</Link>
							<Link
								to="/canvas"
								className="button button--primary button--inverted button--outline"
							>
								Go to the Canvas
							</Link>
						</div>
						<div className={styles.calloutWrapper}>
							<img
								src={CalloutUrl}
								className={styles.callout}
								alt="New? Start here to learn what this is all about."
							/>
						</div>
					</div>
					<div className={styles.bmcExampleWrapper}>
						<img
							src={HeroBmcExampleUrl}
							className={styles.bmcExample}
							alt="An example enering of the Business Model Sustainability Canvas"
						/>
					</div>
				</div>
			</header>
			<main>
				<div className={styles.welcome}>
					<p className={styles.intro}>
						Digital solutions—whether products or services—are essential to
						transforming the aid sector. As humanitarian and development interventions become more
						complex and costly, a business-as-usual approach is no longer feasible. Building
						effective digital solutions in a more sustainable way is necessary to ensure
						cost-effectiveness, scale, and financial sustainability, which in turn supports their
						positive, long-term impact. However, it can be difficult for digital development
						practitioners to apply the Principles for Digital Development to their work and
						investments. In particular, the principle of
						<a href="https://digitalprinciples.org/principle/build-for-sustainability/" target="_blank" rel="noreferrer">Build for Sustainability</a>
						is often an elusive concept that may look different across initiatives.
					</p>
					<h2>What Is the Business Model Sustainability Toolkit?</h2>
					<p>
						The Business Model Sustainability (BMS) Toolkit, funded by Fondation
						Botnar in partnership with the Digital Impact Alliance (DIAL), helps social enterprises,
						NGOs, and small businesses think about their own sustainability and the sustainability
						of their digital solutions. It consists of a Business Model Sustainability Canvas and a
						Business Model Sustainability Guide, which includes guidance, case studies, and
						interactive tools for users.
					</p>
					<h2>What Does the Toolkit Aim to Achieve?</h2>
					<p>
						The goal of the Business Model Sustainability Toolkit is to enable
						intrapreneurs and entrepreneurs in any corner of the world who have developed digital
						solutions to be used for development or humanitarian purposes to create a business
						model to sustain the impact of their solution.
					</p>
					<p>
						An organization utilizing the BMS Toolkit will better understand how it can
						generate and deliver value in a sustainable way, and in turn realize its economic,
						social, and/or environmental impact goals. It will be able to develop business models
						that will enable the sustainable and scalable use of digital programs, tools, or
						products to significantly impact development and humanitarian outcomes.
					</p>
					<div className={styles.callToAction}>
						<Link
							to="/guide"
							className={`
								button
								button--primary
								button--lg
							`}
						>
							Start reading the Guide
						</Link>
					</div>
				</div>
			</main>
		</Layout>
	)
}
