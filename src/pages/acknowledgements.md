---
title: "Acknowledgements"
---

# Acknowledgements

**This project was delivered due to the hard work of the following:**

- The team who developed the content:
	- Ian Gray
	- Cristina Alves
	- [Amos Doornbos](https://www.thisisamos.com)
	- Eliana Fram
	- Matt Haikin
	- Akshika Patel
- The team who managed various aspects of the project:
	- Nicki Ashcroft
	- Steve Conrad
	- Trish Dorsey
	- Michael Downey
	- Laura Walker McDonald
	- Claudine Lim
	- Scott Neilitz
	- Allana Nelson
- The designers and website developers at [Bad Idea Factory](https://www.biffud.com):
	- [Justin Reese](https://justinreese.com) (Technical Project Lead)
	- Dan Schultz (Lead Developer)
	- Margo Dunlap (UX Designer)
	- Annabel Church (Developer)
	- Kavya Sukumar (Developer)
- The additional reference group and reviewers who provided invaluable feedback, insights, and contributions:
	- Heath Arensen
	- Ed Duffus
	- Aru Gurung
	- Angela Odour Lungati
	- Mario Marais
	- Nekesa Were
	- Stuart Davis
	- Brenda Maday
	- John Zoltner
- The original Business Model Canvas that the toolkit is based on:
	- [Strategyzer](https://www.strategyzer.com/canvas/business-model-canvas)
