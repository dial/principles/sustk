import React from 'react'
import PropTypes from 'prop-types'
import {
	TableBody,
} from '@mui/material'
import { productComponentType } from '../../../types'
import {
	COMPONENT_COMPARISON_TYPE_DISPLAY_ORDER,
	PRODUCT_COMPONENT_ADAPTABILITY_TYPE,
} from '../../../constants'
import ComponentComparisonSection from './ComponentComparisonSection'

function shouldShowAddRemoveUi(componentType) {
	switch (componentType) {
	case PRODUCT_COMPONENT_ADAPTABILITY_TYPE.WRAPAROUND:
	case PRODUCT_COMPONENT_ADAPTABILITY_TYPE.NOT_PRESENT:
		return true
	default:
		return false
	}
}

function ComponentComparisonBody({
	productComponents,
	componentComparisons,
	updateComponentComparison,
	updateProductComponent,
	removeProductComponent,
	addProductComponentWithType,
}) {
	return (
		<TableBody>
			{
				COMPONENT_COMPARISON_TYPE_DISPLAY_ORDER.map((componentType) => {
					const productComponentsForType = productComponents.filter(
						(productComponent) => productComponent.now === componentType,
					)

					const componentComparisonsForType = componentComparisons.filter(
						(_, index) => productComponents[index].now === componentType,
					)

					return (
						<ComponentComparisonSection
							key={componentType}
							componentType={componentType}
							productComponentsForType={productComponentsForType}
							componentComparisonsForType={componentComparisonsForType}
							updateProductComponent={updateProductComponent}
							updateComponentComparison={updateComponentComparison}
							removeProductComponent={removeProductComponent}
							addProductComponentWithType={addProductComponentWithType}
							shouldShowAddRemoveUi={shouldShowAddRemoveUi(componentType)}
						/>
					)
				})
			}
		</TableBody>
	)
}

ComponentComparisonBody.propTypes = {
	productComponents: PropTypes.arrayOf(productComponentType).isRequired,
	componentComparisons: PropTypes.arrayOf(
		PropTypes.arrayOf(PropTypes.string),
	).isRequired,
	updateComponentComparison: PropTypes.func.isRequired,
	updateProductComponent: PropTypes.func.isRequired,
	removeProductComponent: PropTypes.func.isRequired,
	addProductComponentWithType: PropTypes.func.isRequired,
}

export default React.memo(ComponentComparisonBody)
