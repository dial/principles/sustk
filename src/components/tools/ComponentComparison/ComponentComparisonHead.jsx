import React from 'react'
import PropTypes from 'prop-types'
import {
	TextField,
	TableHead,
	TableRow,
	TableCell,
} from '@mui/material'
import { competitorType } from '../../../types'
import toolStyles from '../Tools.modules.css'

function ComponentComparisonHead({
	competitors,
	updateCompetitor,
}) {
	const handleUpdatedLabel = (index, event) => {
		const newCompetitor = { ...competitors[index] }
		newCompetitor.label = event.target.value
		updateCompetitor(newCompetitor)
	}

	return (
		<TableHead>
			<TableRow>
				<TableCell component="th" colSpan="2" />
				<TableCell component="th" colSpan="5" className={toolStyles.superHeaderCell}>
					<div className={toolStyles.superHeaderCellInner}>
						Competitors
					</div>
				</TableCell>
				<TableCell component="th" />
			</TableRow>
			<TableRow>
				<TableCell component="th" />
				<TableCell component="th">
					Component
				</TableCell>
				{ competitors.map((competitor, index) => (
					<TableCell
						component="th"
						key={competitor.key}
						className={toolStyles.hasSuperHeader}
					>
						<TextField
							aria-label="Competitor { index }"
							value={competitor.label}
							onChange={(event) => { handleUpdatedLabel(index, event) }}
						/>
					</TableCell>
				))}
				<TableCell component="th" />
			</TableRow>
		</TableHead>
	)
}

ComponentComparisonHead.propTypes = {
	competitors: PropTypes.arrayOf(competitorType).isRequired,
	updateCompetitor: PropTypes.func.isRequired,
}

export default ComponentComparisonHead
