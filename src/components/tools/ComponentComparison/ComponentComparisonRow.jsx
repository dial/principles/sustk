import React from 'react'
import PropTypes from 'prop-types'
import {
	TableRow,
	TableCell,
	TextField,
	IconButton,
} from '@mui/material'
import { XCircleIcon } from '@heroicons/react/outline'
import { productComponentType } from '../../../types'
import SyncedTextAreaAutoresizeSet from '../../generic/SyncedTextareaAutoresizeSet'

function ComponentComparisonRow({
	productComponent,
	componentComparisonsForProductComponent,
	updateProductComponent,
	updateComponentComparison,
	shouldShowRemoveButton,
	removeProductComponent,
}) {
	const handleNameChange = (event) => {
		updateProductComponent({
			...productComponent,
			name: event.target.value,
		})
	}

	const handleUpdatedTextValue = (value, index) => {
		updateComponentComparison(
			index,
			productComponent.key,
			value,
		)
	}

	const handleRemoveClick = () => {
		// This is only temporary until we add a proper MUI Dialog confirmation
		// eslint-disable-next-line no-alert
		if (window.confirm('Are you sure you want to delete this component?')) {
			removeProductComponent(productComponent)
		}
	}

	return (
		<TableRow>
			<TableCell>
				<TextField
					aria-label="Component"
					value={productComponent.name}
					onChange={handleNameChange}
				/>
			</TableCell>
			<SyncedTextAreaAutoresizeSet
				textValues={componentComparisonsForProductComponent}
				updateTextValue={handleUpdatedTextValue}
				minRows={1}
				wrapper={TableCell}
			/>
			<TableCell>
				{ shouldShowRemoveButton ? (
					<IconButton
						aria-label="Delete component"
						onClick={handleRemoveClick}
						size="small"
					>
						<XCircleIcon width="24" />
					</IconButton>
				) : null }
			</TableCell>
		</TableRow>
	)
}

ComponentComparisonRow.propTypes = {
	productComponent: productComponentType.isRequired,
	componentComparisonsForProductComponent: PropTypes.arrayOf(
		PropTypes.string,
	).isRequired,
	updateProductComponent: PropTypes.func.isRequired,
	updateComponentComparison: PropTypes.func.isRequired,
	shouldShowRemoveButton: PropTypes.bool.isRequired,
	removeProductComponent: PropTypes.func.isRequired,
}

export default React.memo(ComponentComparisonRow)
