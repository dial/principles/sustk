/*
	Conforming to line-length requirements for this file, which is essentially an on-disk datastore,
	would be incredibly onerous. Disabling it.
*/
/* eslint-disable max-len */
import React from 'react'

const revenueModelsData = {
	project: {
		revenueModels: [
			{
				id: 'project--1',
				title: 'Innovation Program Grant',
				summary: 'Innovation program grants are funds provided by donors (and sometimes investors) to develop your digital solution across the innovation cycle, specifically the prototyping, piloting, and scaling phases.',
				detail: (
					<>
						<p>Innovation program grants are funds provided by donors (and sometimes investors) to develop your digital solution across the innovation cycle, specifically the prototyping, piloting, and scaling phases.</p>
						<p>They are typically for time and materials but often have more flexibility than other aid funding, so that a proportion of funds can be spent on product and organizational development. Some grants can be focused on specific areas of innovation, such as generating research and evidence.</p>
						<p>These types of grants will only be available while your digital solution is seen as an innovation. Once your digital solution is seen as more mature, it will no longer be eligible for such grants. Be aware that such funding is also exceptionally competitive. You will need to find a way of graduating from this type of funding to more mainstream types of funding as your solution matures.</p>
					</>
				),
			},
			{
				id: 'project--2',
				title: 'Grants/Contracts and Subcontracting',
				summary: 'Grants and contracts and subgrants and subcontracts are the main funding mechanisms in the aid sector. They are usually only available to you once your digital solution has been piloted in at least one location and you have evidence that your solution is creating social, developmental, or humanitarian impact.',
				detail: (
					<>
						<p>Grants and contracts and subgrants and subcontracts are the main funding mechanisms in the aid sector. They are usually only available to you once your digital solution has been piloted in at least one location and you have evidence that your solution is creating social, developmental, or humanitarian impact (see Value Proposition: Evidence of Impact). They are provided to implement a time-bound project in one or more locations, and they are primarily sector specific (e.g., a health or education project) but can be multisectoral.</p>
						<p>For these funding streams, you are most likely to be a subcontractor or subgrantee. A back donor or finance ministry will provide funding to an aid agency or government department for the project, and they will then subcontract you to deliver part of the project through your digital solution. Another element of subcontracting is partner revenue share.</p>
						<p>The key points to be aware of for this revenue stream are:</p>
						<ol>
							<li>Time and materials-based contracts are unlikely to cover working capital for ongoing development costs for your digital solution.</li>
							<li>Payment by deliverable can provide more flexible funding for working capital.</li>
							<li>If you are a subgrantee, you can negotiate for a percentage of indirect cost recovery (overhead) that will be part of the primary grant recipient. However, the primary grantee or contractor is unlikely to pass any of these fungible funds on to you in your subgrant or subcontract.</li>
						</ol>
						<p>This revenue stream has significant impacts on cash flow because:</p>
						<ol>
							<li>The time between verbal agreement and signed contracts will often take months.</li>
							<li>The time between the delivery of milestones or reports and funding can take longer than four weeks, so ensure that you are clear on payment terms.</li>
							<li>Payment in arrears can significantly impact your cash flow.</li>
							<li>There can be significant gaps between grants.</li>
						</ol>
					</>
				),
			},
		],
	},
	product: {
		revenueModels: [
			{
				id: 'product--1',
				title: 'White Labelling',
				summary: 'The white labelling model allows developers to sell their products to organizations who can then rebrand the product.',
				detail: (
					<>
						<p>
							The white labelling model allows developers to sell their products to organizations who can then rebrand the product.
							<sup>1</sup>
							{' '}
							While this model is not specific to the technology sector, it largely takes place in the information technology and e-commerce industries.
							<sup>2</sup>
							{' '}
							This is due to a couple of factors. First, the model allows organizations to offer a comprehensive set of solutions to their target impact groups without the need to focus on developing their own solutions and housing them. Secondly, by implementing the white labelling model, an organization positions itself as a knowledge expert.
							<sup>3</sup>
						</p>
						<div className="admonition admonition-info alert alert--info">
							<div className="admonition-heading">
								<h5>
									<span className="admonition-icon"><svg xmlns="http://www.w3.org/2000/svg" width="14" height="16" viewBox="0 0 14 16"><path fillRule="evenodd" d="M7 2.3c3.14 0 5.7 2.56 5.7 5.7s-2.56 5.7-5.7 5.7A5.71 5.71 0 0 1 1.3 8c0-3.14 2.56-5.7 5.7-5.7zM7 1C3.14 1 0 4.14 0 8s3.14 7 7 7 7-3.14 7-7-3.14-7-7-7zm1 3H6v5h2V4zm0 6H6v2h2v-2z" /></svg></span>
									Case Study: White Label Model
								</h5>
							</div>
							<div className="admonition-content">
								<p>
									<a href="https://www.redrosecps.com" target="_blank" rel="noopener noreferrer">Red Rose</a>
									{' '}
									is a digital solution for cash and voucher programs for social safety net and humanitarian programming. One of its revenue streams has been through whitelabelling its product, allowing the buyer to use its own branding throughout. This is something that the International Committee of the Red Cross (ICRC) did to integrate the Red Rose platform into its cash programming.
								</p>
							</div>
						</div>
						<div className="footnotes">
							<hr />
							<ol>
								<li>
									Andony, B.
									{' '}
									<a href="https://www.vendasta.com/blog/white-label-vs-private-label/" target="_blank" rel="noopener noreferrer">“White-Label vs Private-Label.”</a>
									{' '}
									Vendasta Blog (blog), August 24, 2018.
								</li>
								<li>
									Business Model Navigator.
									{' '}
									<a href="https://businessmodelnavigator.com/pattern?id=55" target="_blank" rel="noopener noreferrer">“Whitelabel Business Model Pattern.”</a>
									{' '}
									Accessed April 12, 2021.
								</li>
								<li>
									Coleman, R.
									{' '}
									<a href="https://search.proquest.com/docview/1750091006/abstract/D544BB61B17C4FC0PQ/1" target="_blank" rel="noopener noreferrer">“White Label: The Fastest Growing Business Cloud Model.”</a>
									{' '}
									Channel Pro. London, United Kingdom: Dennis Publishing Ltd., December 18, 2015.
								</li>
							</ol>
						</div>
					</>
				),
			},
			{
				id: 'product--2',
				title: 'Variable- or Unit-Based Model',
				summary: 'A variable- or unit-based model can be divided into two types. The first is the most traditional selling model, in which each unit is sold as an individual or bundled item and there is a reliance on repeat purchases. The second type is when a service is paid for as part of an ongoing contract, but the payment is based on the amount of units used in a certain time period.',
				detail: (
					<>
						<p>A variable- or unit-based model can be divided into two types. The first is the most traditional selling model, in which each unit is sold as an individual or bundled item and there is a reliance on repeat purchases. The second type is when a service is paid for as part of an ongoing contract, but the payment is based on the amount of units used in a certain time period. The second type is the most common for digital solutions. Typical units would be:</p>
						<ol>
							<li>Number of times used - Often used for digital data gathering services</li>
							<li>Number of people in the target impact segment reached - sometimes referred to as beneficiaries</li>
							<li>Number of users - Almost always used with a licensing or SaaS model</li>
						</ol>
					</>
				),
			},
			{
				id: 'product--3',
				title: 'Software as a Service Subscription Model',
				summary: 'The SaaS subscription model allows the user to access software through an internet browser on a pay-for-use basis. As such, the user does not own the software, rather an external provider owns and manages the software.',
				detail: (
					<>
						<p>
							Unlike software that a user downloads onto their own personal device, the SaaS subscription model allows the user to access software through an internet browser on a pay-for-use basis.
							<sup>1</sup>
							{' '}
							As such, the user does not own the software, rather an external provider owns and manages the software. SaaS is more user friendly than the traditional software model in terms of accessibility, updates, hardware, and storage, with the latter three managed by the provider. Key benefits for the provider include market research and data analytics. SaaS allows providers to easily expand their customer base.
							<sup>2</sup>
							{' '}
							Additionally, since providers control and manage all data, data analytics are an easier endeavor. The biggest benefit is that SaaS only requires access to an internet connection.
						</p>
						<p>There are significant factors you need to consider when using this model in humanitarian and development contexts:</p>
						<ol>
							<li>Data ownership and protection</li>
							<li>Connectivity: Using cloud-based services can be difficult in low-connectivity environments</li>
							<li>Lock-in ethics: There are potential ethical issues with small nonprofit organizations being locked in to a single provider for the service</li>
						</ol>
						<div className="admonition admonition-info alert alert--info">
							<div className="admonition-heading">
								<h5>
									<span className="admonition-icon"><svg xmlns="http://www.w3.org/2000/svg" width="14" height="16" viewBox="0 0 14 16"><path fillRule="evenodd" d="M7 2.3c3.14 0 5.7 2.56 5.7 5.7s-2.56 5.7-5.7 5.7A5.71 5.71 0 0 1 1.3 8c0-3.14 2.56-5.7 5.7-5.7zM7 1C3.14 1 0 4.14 0 8s3.14 7 7 7 7-3.14 7-7-3.14-7-7-7zm1 3H6v5h2V4zm0 6H6v2h2v-2z" /></svg></span>
									Case Study: Case Study: Subscription SaaS Model
								</h5>
							</div>
							<div className="admonition-content">
								<p>
									<a href="https://www.simprints.com" target="_blank" rel="noopener noreferrer">Simprints</a>
									{' '}
									is a nonprofit technology startup from the University of Cambridge building biometrics for target impact segment identification in developing countries. Its SaaS operating model enables customers to integrate biometrics collection as part of their work. Simprints manages the technology, including the storage of biometrics. It provides training to data collectors and support to the organizations, but it manages the backend technology. Pricing is done based on usage and project size.
								</p>
								<p>Biometrics are notoriously difficult to share due to the lack of interoperability across different biometric providers, which causes vendor lock-in. However, this is less about the business model and more about the maturity of the technology.</p>
							</div>
						</div>
						<div className="footnotes">
							<hr />
							<ol>
								<li>
									Gartner.
									{' '}
									<a href="https://www.gartner.com/en/information-technology/glossary/software-as-a-service-saas" target="_blank" rel="noopener noreferrer">“Definition of Software as a Service (SaaS) - Gartner Information Technology Glossary.”</a>
									{' '}
									Accessed April 8, 2021.
								</li>
								<li>
									Turner, B. (2020).
									{' '}
									<a href="https://www.techradar.com/news/what-is-saas" target="_blank" rel="noopener noreferrer">“What Is SaaS? Everything You Need to Know about Software as a Service.”</a>
									{' '}
									TechRadar. Accessed April 8, 2021.
								</li>
							</ol>
						</div>
					</>
				),
			},
			{
				id: 'product--4',
				title: 'Licensing Model',
				summary: 'The licensing model is based on the idea that the end user purchases the right to use software on a limited basis rather than purchasing the software outright.',
				detail: (
					<>
						<p>
							The licensing model is based on the idea that the end user purchases the right to use software on a limited basis rather than purchasing the software outright.
							<sup>1</sup>
							{' '}
							There are multiple types of licensing models available.The four most common include end-user license agreement (ELA), pay per user, public domain, sharing license, and site license.
							<sup>2</sup>
							{' '}
							Of all the various types of licensing models available, end-user license agreements are the most common. With ELAs, software is physically installed on one device with its own unique license code.
						</p>
						<p>
							Per server licensing entails the supplier installing software on a server as opposed to a device. With concurrent user licensing, the end user purchases enough licenses to match the number of users at one time point. Nonperpetual licensing, on the other hand, entails the user prepaying for a software license for a specific time period. A common denominator among this diverse set of licensing models is that the manufacturer gives the end user the software key.
							<sup>3</sup>
						</p>
						<p>Licensing for local versions of the software is more useful for country contexts where there are connectivity issues because of coverage or cost.</p>
						<div className="admonition admonition-info alert alert--info">
							<div className="admonition-heading">
								<h5>
									<span className="admonition-icon"><svg xmlns="http://www.w3.org/2000/svg" width="14" height="16" viewBox="0 0 14 16"><path fillRule="evenodd" d="M7 2.3c3.14 0 5.7 2.56 5.7 5.7s-2.56 5.7-5.7 5.7A5.71 5.71 0 0 1 1.3 8c0-3.14 2.56-5.7 5.7-5.7zM7 1C3.14 1 0 4.14 0 8s3.14 7 7 7 7-3.14 7-7-3.14-7-7-7zm1 3H6v5h2V4zm0 6H6v2h2v-2z" /></svg></span>
									Case Study: Licensing Model
								</h5>
							</div>
							<div className="admonition-content">
								<p>
									Last Mile Mobile Solutions (
									<a href="https://lmms.org" target="_blank" rel="noopener noreferrer">LMMS</a>
									) is a suite of tools—from beneficiary management to digital cash vouchers—for implementing projects at the last mile of development and aid delivery. It was developed by World Vision in 2008 and is licensed to other agencies for use. The LMMS licensing methodology has evolved over time. LMMS has used a flat fee annual license for unlimited usage, a project license, and a per-beneficiary license. The evolution of the licensing model was done in conversations with its customers with the intent of making it easier for customers to allocate costs to their projects.
								</p>
							</div>
						</div>
						<div className="footnotes">
							<hr />
							<ol>
								<li>
									Bourne, K. C.
									{' '}
									<a href="https://doi.org/10.1016/B978-0-12-398545-3.00021-2" target="_blank" rel="noopener noreferrer">“Leveraging the Vendor Relationship.”</a>
									{' '}
									In Application Administrators Handbook, 361–76. Elsevier, 2014.
								</li>
								<li>Ibid.</li>
								<li>
									GmbH, Labs64.
									{' '}
									<a href="https://netlicensing.io/blog/2013/06/13/software-licensing-models-types-sizes-and-uses/" target="_blank" rel="noopener noreferrer">“Software Licensing Models – Types, Sizes and Uses | NetLicensing.”</a>
									{' '}
									Text. February 1, 2021.
									{' '}
								</li>
							</ol>
						</div>
					</>
				),
			},
			{
				id: 'product--5',
				title: 'Customization',
				summary: 'Customization refers to the act of altering a product or service to meet certain preferences or requirements and can provide revenue if you charge for these changes. Buyers often want modifications to your digital solution.',
				detail: (
					<>
						<p>
							Customization refers to the act of altering a product or service to meet certain preferences or requirements and can provide revenue if you charge for these changes. As noted in the
							{' '}
							<a href="/guide/customer-relationships/">Customer Relationships</a>
							{' '}
							section, buyers often want modifications to your digital solution. You need to be clear with them that this is an additional paid service and any modifications will need to be fully funded, including management costs. Although this is one of the more common revenue streams, you should carry out a cost-benefit analysis on whether the work is worth it, based on your ability to leverage the work for other customers and the likely impact on the developer’s and other team members’ time.
						</p>
						<div className="admonition admonition-info alert alert--info">
							<div className="admonition-heading">
								<h5>
									<span className="admonition-icon"><svg xmlns="http://www.w3.org/2000/svg" width="14" height="16" viewBox="0 0 14 16"><path fillRule="evenodd" d="M7 2.3c3.14 0 5.7 2.56 5.7 5.7s-2.56 5.7-5.7 5.7A5.71 5.71 0 0 1 1.3 8c0-3.14 2.56-5.7 5.7-5.7zM7 1C3.14 1 0 4.14 0 8s3.14 7 7 7 7-3.14 7-7-3.14-7-7-7zm1 3H6v5h2V4zm0 6H6v2h2v-2z" /></svg></span>
									Case Study: Customization
								</h5>
							</div>
							<div className="admonition-content">
								<p>
									Elements of
									{' '}
									<a href="https://lmms.org" target="_blank" rel="noopener noreferrer">LMMS</a>
									{' '}
									are configurable, meaning that small changes can be made to fit the organization or project. However, sometimes a customer wants something that is not available or a significant change to one of the tools. For example, a microfinance institution wanted to use LMMS to digitize its loan application and approval processes. LMMS had the foundational requirements for this (i.e., registration, form collection, and connections to FSPs). Together, LMMS and the microfinance institution mapped out the new requirements and designed workflows. Costings were done and the MFI paid for the new custom solution.
								</p>
							</div>
						</div>
					</>
				),
			},
			{
				id: 'product--6',
				title: 'Complementary Services',
				summary: 'Offering complementary services is the most common approach for digital solutions in the aid sector. These services are often in the form of support for on-premise deployments, consultancy services such as data analysis and coaching, and training opportunities.',
				detail: (
					<>
						<p>Offering complementary services is the most common approach for digital solutions in the aid sector. These services are often in the form of support for on-premise deployments, consultancy services such as data analysis and coaching, and training opportunities. In the development and humanitarian sectors, low levels of digital literacy make complementary services necessary. This may also mean that a significant amount of handholding will be required by organizations to successfully deploy and implement digital products and services, and you may be required to take this on. A detailed list of potential complementary service options is found in the table below.</p>
						<table>
							<thead>
								<tr>
									<th>Revenue Model</th>
									<th>Description</th>
								</tr>
							</thead>
							<tbody>
								<tr>
									<td>Support of on-premise deployments</td>
									<td>Project provides support services for an installation of the software that has been procured and deployed on its own or through third-party servers.</td>
								</tr>
								<tr>
									<td>Professional services</td>
									<td>Paid consulting services provided on a contract basis to consumers of the product, such as data analysis and coaching.</td>
								</tr>
								<tr>
									<td>Support retainer</td>
									<td>Amount of money paid in advance by a client to assure the maintainer’s services will be available to them for a period of time.</td>
								</tr>
								<tr>
									<td>Training fees</td>
									<td>organized paid activity aimed to help consumers of open source attain proficiency with the maintainer’s software.</td>
								</tr>
							</tbody>
							<tfoot>
								<tr>
									<td colSpan="2">
										<small>
											Source:
											{' '}
											<a href="https://digitalimpactalliance.org/wp-content/uploads/2021/01/Sustainability-Guidbook.pdf" target="_blank" rel="noopener noreferrer">Open-Source Sustainability Toolkit</a>
										</small>
									</td>
								</tr>
							</tfoot>
						</table>
						<p>Although this form of revenue stream is common, it can come with significant transaction costs, is often based upon time and materials contracts that are hard to generate a surplus from, and is not particularly scalable.</p>
						<div className="admonition admonition-info alert alert--info">
							<div className="admonition-heading">
								<h5>
									<span className="admonition-icon"><svg xmlns="http://www.w3.org/2000/svg" width="14" height="16" viewBox="0 0 14 16"><path fillRule="evenodd" d="M7 2.3c3.14 0 5.7 2.56 5.7 5.7s-2.56 5.7-5.7 5.7A5.71 5.71 0 0 1 1.3 8c0-3.14 2.56-5.7 5.7-5.7zM7 1C3.14 1 0 4.14 0 8s3.14 7 7 7 7-3.14 7-7-3.14-7-7-7zm1 3H6v5h2V4zm0 6H6v2h2v-2z" /></svg></span>
									Case Study: Consultancy Model
								</h5>
							</div>
							<div className="admonition-content">
								<p>
									<a href="https://www.dimagi.com/services/" target="_blank" rel="noopener noreferrer">Dimagi</a>
									{' '}
									is best known for its
									{' '}
									<a href="https://www.dimagi.com/commcare/" target="_blank" rel="noopener noreferrer">CommCare</a>
									{' '}
									solution, which it charges a monthly fee for. However, Dimagi complements this revenue stream by offering various
									{' '}
									<a href="https://www.dimagi.com/commcare/onboarding/" target="_blank" rel="noopener noreferrer">onboarding packages</a>
									. These packages serve multiple functions. First, they are an additional revenue stream. Second, they complement all the free Dimagi resources available to customers, helping to ensure the customer is successful in using CommCare. This is in the interest of the customer but also in the long-term interests of Dimagi. Customers tell others about the support they received from Dimagi, not necessarily that they paid extra for it.
								</p>
							</div>
						</div>
					</>
				),
			},
		],
	},
	indirect: {
		revenueModels: [
			{
				id: 'indirect--1',
				title: 'Freemium',
				summary: 'In a freemium model, a basic version of the service is provided for free, with the intent of attracting a percentage of users who will purchase a premium version of the software. The key to the success of this model is the number of free users you can gain and the percentage of those free users that you can convert into paying premium customers to generate revenue that will cross-finance the free version.',
				detail: (
					<>
						<p>In a freemium model, a basic version of the service is provided for free, with the intent of attracting a percentage of users who will purchase a premium version of the software. The key to the success of this model is the number of free users you can gain and the percentage of those free users that you can convert into paying premium customers to generate revenue that will cross-finance the free version.</p>
						<p>In the humanitarian and development sectors, freemium models are more like loss leader methods, where you absorb the initial costs of deployment or usage during an extended trial in order to gain a purchase of the product or to get paid for additional complementary services, such as training and consultancy. However, there is the possibility of applying graded charging, where you charge based on the annual turnover of the buying organization. For example, a large UN body pays a premium and a small local CSO pays a nominal fee.</p>
					</>
				),
			},
			{
				id: 'indirect--2',
				title: 'Robin Hood',
				summary: 'In a Robin Hood model, the solution is sold to one customer segment at a much higher price and to another customer segment at a lower price. This can be the same or a similar product for each segment.',
				detail: (
					<>
						<p>In a Robin Hood model, the solution is sold to one customer segment at a much higher price and to another customer segment at a lower price. This can be the same or a similar product for each segment. In this model, you are providing a version of your service to one market (most likely the private sector) and another version of the service to the aid sector.</p>
						<div className="admonition admonition-info alert alert--info">
							<div className="admonition-heading">
								<h5>
									<span className="admonition-icon"><svg xmlns="http://www.w3.org/2000/svg" width="14" height="16" viewBox="0 0 14 16"><path fillRule="evenodd" d="M7 2.3c3.14 0 5.7 2.56 5.7 5.7s-2.56 5.7-5.7 5.7A5.71 5.71 0 0 1 1.3 8c0-3.14 2.56-5.7 5.7-5.7zM7 1C3.14 1 0 4.14 0 8s3.14 7 7 7 7-3.14 7-7-3.14-7-7-7zm1 3H6v5h2V4zm0 6H6v2h2v-2z" /></svg></span>
									Case Study: Robin Hood
								</h5>
							</div>
							<div className="admonition-content">
								<p>
									<a href="https://smap.com.au/" target="_blank" rel="noopener noreferrer">SMAP FieldTask</a>
									{' '}
									is a digital data collection tool built on
									{' '}
									<a href="https://getodk.org/" target="_blank" rel="noopener noreferrer">ODK</a>
									. SMAP provides digital data gathering services to humanitarian and development agencies. It has managed to sustain a business model of delivering these services at a subsidized rate for more than a decade. SMAP is able to provide these services to the aid sector at a lower cost because of the revenue it generates from providing digital data gathering services to private-sector companies.
								</p>
							</div>
						</div>
					</>
				),
			},
			{
				id: 'indirect--3',
				title: 'Leveraging Core Capabilities ',
				summary: 'Leveraging core capabilities involves identifying core capabilities that you have developed and packaging them into a service offering for a different market.',
				detail: (
					<>
						<p>Leveraging core capabilities involves identifying core capabilities that you have developed and packaging them into a service offering for a different market. This is not repackaging the digital solution you have for another market. Rather, it is repackaging the processes, services, or systems you have developed in the process of designing and delivering your digital solution. The idea is to leverage this capability in a more profitable market.</p>
						<div className="admonition admonition-info alert alert--info">
							<div className="admonition-heading">
								<h5>
									<span className="admonition-icon"><svg xmlns="http://www.w3.org/2000/svg" width="14" height="16" viewBox="0 0 14 16"><path fillRule="evenodd" d="M7 2.3c3.14 0 5.7 2.56 5.7 5.7s-2.56 5.7-5.7 5.7A5.71 5.71 0 0 1 1.3 8c0-3.14 2.56-5.7 5.7-5.7zM7 1C3.14 1 0 4.14 0 8s3.14 7 7 7 7-3.14 7-7-3.14-7-7-7zm1 3H6v5h2V4zm0 6H6v2h2v-2z" /></svg></span>
									Case Study: Leveraging Core Capabilities
								</h5>
							</div>
							<div className="admonition-content">
								<p>
									<a href="https://www.snhu.edu" target="_blank" rel="noopener noreferrer">Southern New Hampshire University</a>
									{' '}
									(SNHU) is a nonprofit, regionally accredited university with an 80-year history of providing high-quality education to students online and on campus. It has more than 3,000 in-person students and offers affordable, accessible degree programs to more than 135,000 students online.
								</p>
								<p>
									The
									{' '}
									<a href="https://gem.snhu.edu/about-gem/" target="_blank" rel="noopener noreferrer">Global Education Movement</a>
									{' '}
									(GEM) is a major SNHU initiative offering university degrees to refugees around the world. Created in 2017, GEM is the first large-scale online learning initiative for refugees, partnering with in-country organizations to deliver high-quality, low-cost education tailored to meet the needs of displaced learners.
								</p>
								<p>In order to reduce the costs of an online degree, GEM has developed a hub in Rwanda that provides assessment and IT support for SNHU. The hub has been so successful that it has enabled GEM to provide these services to other North American educational institutions, providing a revenue stream to cross-subsidize its online learning programs for refugee learners.</p>
							</div>
						</div>
					</>
				),
			},
			{
				id: 'indirect--4',
				title: 'Advertising',
				summary: 'Users of a digital product are shown advertisements within the product itself. Revenue is often generated each time the user clicks on the ad or from corporate sponsorships.',
				detail: (
					<>
						<p>In the advertising revenue stream, users of a digital product are shown advertisements within the product itself. Revenue is often generated each time the user clicks on the ad or from corporate sponsorships. This model can be used as a complementary funding stream to freemium services. For multimedia products, advertising can be embedded into the product, such as putting ads in an online training video. It is primarily an income stream that works when you have a large group of users who are most likely to be your target impact segment.</p>
						<p>Be sure you are not advertising products and services that are contrary to your stated mission or ethics.</p>
					</>
				),
			},
			{
				id: 'indirect--5',
				title: 'Microtasking',
				summary: 'Microtasking is the process of completing a simple task for a small payment (such as confirming what objects are in a picture to train Artificial Intelligence).',
				detail: (
					<>
						<p>While advertising relies on eyes to view content, microtasking relies on brains to carry out a small task. Microtasking is the process of completing a simple task for a small payment (such as confirming what objects are in a picture to train Artificial Intelligence). You can use microtasking to generate funding by having users carry out a small task (i.e., identify an object in an image to train AI) to access your content. The businesses that need these tasks completed then pay you for each successful task completed.</p>
						<div className="admonition admonition-info alert alert--info">
							<div className="admonition-heading">
								<h5>
									<span className="admonition-icon"><svg xmlns="http://www.w3.org/2000/svg" width="14" height="16" viewBox="0 0 14 16"><path fillRule="evenodd" d="M7 2.3c3.14 0 5.7 2.56 5.7 5.7s-2.56 5.7-5.7 5.7A5.71 5.71 0 0 1 1.3 8c0-3.14 2.56-5.7 5.7-5.7zM7 1C3.14 1 0 4.14 0 8s3.14 7 7 7 7-3.14 7-7-3.14-7-7-7zm1 3H6v5h2V4zm0 6H6v2h2v-2z" /></svg></span>
									Case Study: Advertising and Microtasking
								</h5>
							</div>
							<div className="admonition-content">
								<p>
									<a href="https://brck.com/moja/" target="_blank" rel="noopener noreferrer">Moja</a>
									{' '}
									is a free public Wi-Fi service that started in Nairobi and has spread across Kenya and beyond. It is used on public transport and in public spaces. Moja provides free Wi-Fi to users in exchange for viewing ads or carrying out microtasks. This has brought free Wi-Fi to low-income people. The advertising and microtasking comes from businesses that cross-subsidize the free use of Wi-Fi for the target impact segment.
								</p>
							</div>
						</div>
					</>
				),
			},
			{
				id: 'indirect--6',
				title: 'Data Monetisation',
				summary: 'Data monetisation is the act of turning data into currency, which can be in the form of actual money. It also refers to data as a bartering device or a product or service enhancement.',
				detail: (
					<>
						<p>Data monetisation is the act of turning data into currency, which can be in the form of actual money. It also refers to data as a bartering device or a product or service enhancement. This form of revenue stream is particularly ethically sensitive and should only be used if it does not exploit or compromise the privacy of your users’ data. An example of this model is the sale of depersonalized agricultural data collected by smallholder farmers to sell to insurance, futures, and food industry markets.</p>
						<div className="admonition admonition-info alert alert--info">
							<div className="admonition-heading">
								<h5>
									<span className="admonition-icon"><svg xmlns="http://www.w3.org/2000/svg" width="14" height="16" viewBox="0 0 14 16"><path fillRule="evenodd" d="M7 2.3c3.14 0 5.7 2.56 5.7 5.7s-2.56 5.7-5.7 5.7A5.71 5.71 0 0 1 1.3 8c0-3.14 2.56-5.7 5.7-5.7zM7 1C3.14 1 0 4.14 0 8s3.14 7 7 7 7-3.14 7-7-3.14-7-7-7zm1 3H6v5h2V4zm0 6H6v2h2v-2z" /></svg></span>
									Case Study: Data Monetisation
								</h5>
							</div>
							<div className="admonition-content">
								<p>
									SPARK IGNITE Rwanda has introduced a new technology platform called Mpuzanunguke Kirayi through its
									{' '}
									<a href="https://www.agroberichtenbuitenland.nl/binaries/agroberichtenbuitenland/documenten/nieuwsbrieven/2019/06/30/newsletter-kig-lan-april-june-2019/Newsletter+KIG-LAN+April+-+June.pdf" target="_blank" rel="noopener noreferrer">Irish Potato Value Chain Financing</a>
									{' '}
									(IPoVaF) project. The platform helps potato farmers carry out various bank transactions on their mobile devices, including applying for loans. Farmers are asked to record their produce data on a seasonal or daily basis, which not only boosts their credit worthiness, but also allows financial institutions to analyze these transactions and determine the farmer’s financial capacity to pay back a loan.
								</p>
								<p>
									<small>
										Taken from
										{' '}
										<a href="https://www.newtimes.co.rw/news/new-mobile-app-boost-potato-farming" target="_blank" rel="noopener noreferrer">https://www.newtimes.co.rw/news/new-mobile-app-boost-potato-farming</a>
									</small>
								</p>
							</div>
						</div>
					</>
				),
			},
		],
	},
	fundraising: {
		revenueModels: [
			{
				id: 'fundraising--1',
				title: 'Traditional Direct Fundraising',
				summary: 'There are a number of traditional direct fundraising models, including soliciting donations from members of the public through methods such as direct mail, email, and online video and receiving contributions from corporations.',
				detail: (
					<p>There are a number of traditional direct fundraising models, including soliciting donations from members of the public through methods such as direct mail, email, and online video and receiving contributions from corporations. Hosting events such as dinners, silent auctions, and concerts are other methods. Some corporations can help fund these events and receive recognition in return. Many countries require your organization or a part of it to be registered as a charity in order to hold these types of fundraising.</p>
				),
			},
			{
				id: 'fundraising--2',
				title: 'Crowdfunding',
				summary: 'Crowdfunding is the use of a platform to raise money to develop a product or service.',
				detail: (
					<>
						<p>Crowdfunding is the use of a platform to raise money to develop a product or service. This can involve providing funders with your product or service at a discounted price, or you can simply accept the funds as a donation. Developing a crowdfunding campaign is a significant undertaking that requires a concerted social media campaign for it to be successful. Key platforms include Kickstarter, Indiegogo, and GoFundMe.</p>
						<p>For the humanitarian and development sectors, most of the funding from crowdfunding platforms to date has gone to individual projects. However, there is potential for organizations to use this method if they are able to clearly articulate the benefits of their digital solutions and services for the target impact segment. There is also a long history of using this model for digital solutions on platforms such as Patreon, Open Collective, Github Sponsors, and LF Community Bridge.</p>
					</>
				),
			},
		],
	},
	investment: {
		revenueModels: [
			{
				id: 'investment--1',
				title: 'Innovation Grants',
				summary: 'Innovation grants can have a component that is nonprojectized and designed to build the business model. This is often in the form of a percentage of the grant that can be used for business model building activities such as branding, PR, and core staff recruitment.',
				detail: (
					<>
						<p>Innovation grants can have a component that is nonprojectized and designed to build the business model. This is often in the form of a percentage of the grant that can be used for business model building activities such as branding, PR, and core staff recruitment. Being able to use such funds, particularly for staff and overhead, is key to the business model-building activities that will not be covered by your other revenue streams in the early stages of your digital product or service.</p>
						<p>Ensure that you maximize any portion of undesignated funds from such donors and ensure that they are used as much as possible for one-off setup costs. If they are used for recurring expenditure such as staff, have a plan in place for how these positions will be funded once the initial investment has been exhausted.</p>
					</>
				),
			},
			{
				id: 'investment--2',
				title: 'Core Funding',
				summary: 'Core funds can be solely concentrated on the overhead and set-up costs of nonprofit startups, or even more established nonprofit organizations, to cover difficult-to-fund aspects of their business model.',
				detail: (
					<>
						<p>
							Although institutional government funders in North America and Europe have generally moved away from providing core grants to charitable organizations, there are a number of foundations and philanthropists, such as the
							{' '}
							<a href="https://skoll.org/about/skoll-awards/" target="_blank" rel="noopener noreferrer">Skoll Foundation</a>
							, that still provide such funding to organizations. Core funds can be solely concentrated on the overhead and set-up costs of nonprofit startups, or even more established nonprofit organizations, to cover difficult-to-fund aspects of their business model.
						</p>
						<p>As with other funds that cover such costs, you will need to ensure that your business model can be sustainable in the long term without this type of funding. But any such opportunities should be prioritized when it comes to your fundraising and revenue strategy.</p>
					</>
				),
			},
			{
				id: 'investment--3',
				title: 'Fellowships',
				summary: 'Fellowships generally refers to funding given to entrepreneurs that covers their salary and the key activities they seek to achieve.',
				detail: (
					<>
						<p>Some philanthropists, foundations, and innovation funders will provide fellowships, which generally refers to funding given to entrepreneurs that covers their salary and the key activities they seek to achieve. These funders understand the importance of investing in people rather than a single innovation to ensure that great ideas and potential leaders can have the greatest impact on the world.</p>
						<p>Each type of fellowship is different, but they often support the lead entrepreneur and cover some of the expenses of getting their idea off the ground, including aspects of developing the business model.</p>
						<p>
							Examples of global fellowships are from
							{' '}
							<a href="https://www.ashoka.org/en-in/program/ashoka-fellowship" target="_blank" rel="noopener noreferrer">Ashoka</a>
							{' '}
							and the
							{' '}
							<a href="https://shuttleworthfoundation.org/thinking/2017/09/14/thinking-do-it/" target="_blank" rel="noopener noreferrer">Shuttleworth Foundation</a>
							. There are also regional fellowships, such as the
							{' '}
							<a href="https://www.opportunitiesforyouth.org/2021/10/11/apply-now-africa-innovation-fellowship-2021-for-women-entrepreneurs" target="_blank" rel="noopener noreferrer">Africa Innovation Fellowship</a>
							{' '}
							for women entrepreneurs.
						</p>
					</>
				),
			},
			{
				id: 'investment--4',
				title: 'Social Impact Investing',
				summary: 'Social impact investing expects both social impact and financial returns. The types of funds available can be classified into three areas: equity, debt, and hybrids and alternatives.',
				detail: (
					<>
						<p>Social impact investing expects both social impact and financial returns. The types of funds available can be classified into three areas: equity, debt, and hybrids and alternatives.</p>
						<ol>
							<li>
								<b>Equity</b>
								{' '}
								- This is only open if you are a private company that has shares you can offer for the investment.
							</li>
							<li>
								<b>Debt</b>
								{' '}
								- This is where the investment is effectively a loan, often at preferential interest rates.
							</li>
							<li>
								<b>Hybrids and alternatives</b>
								{' '}
								- These can be linked to project outcomes such as social impact bonds or for core funding through blended finance (i.e., part grant and part loan).
							</li>
						</ol>
						<p>A key element to remember is that an investment like this will almost always seek a financial return.</p>
						<div className="alert alert--info">
							<b>Key Resource:</b>
							{' '}
							Good Finance has a useful list of social impact
							{' '}
							<a href="https://www.goodfinance.org.uk/understanding-social-investment/types-social-investment" target="_blank" rel="noopener noreferrer">investment types</a>
							. While it is UK-centric, a number of the investment approaches are used in numerous countries.
						</div>
					</>
				),
			},
		],
	},
}

export default revenueModelsData
