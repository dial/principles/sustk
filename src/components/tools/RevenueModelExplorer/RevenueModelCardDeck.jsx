import React from 'react'
import PropTypes from 'prop-types'
import revenueModelsData from './revenueModelsData'
import RevenueModelCard from './RevenueModelCard'
import styles from './RevenueModelCardDeck.module.css'

function RevenueModelCardDeck({ revenueModelCategoryId }) {
	const revenueModelCategory = revenueModelsData[revenueModelCategoryId]

	return revenueModelCategory ? (
		<div className={styles.deck}>
			{revenueModelCategory.revenueModels.map((revenueModel) => (
				<RevenueModelCard
					key={revenueModel.id}
					revenueModel={revenueModel}
					revenueModelCategoryId={revenueModelCategoryId}
				/>
			))}
		</div>
	) : null
}

RevenueModelCardDeck.propTypes = {
	revenueModelCategoryId: PropTypes.string.isRequired,
}

export default RevenueModelCardDeck
