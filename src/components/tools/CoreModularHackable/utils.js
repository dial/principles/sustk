import { v4 as uuidv4 } from 'uuid'

export function generateProductComponent(id) {
	return {
		key: uuidv4(),
		id,
		name: '',
		now: '',
		future: '',
		actions: '',
		currentAutomationLevel: '',
		targetAutomationLevel: '',
		automationLevelTimeline: '',
	}
}

export function generateProductComponents(count, firstId) {
	return Array.from(
		{ length: count },
		(_, index) => generateProductComponent(index + firstId),
	)
}

export function getProductComponentIndex(productComponent, productComponents) {
	return productComponents.findIndex((item) => productComponent.id === item.id)
}
