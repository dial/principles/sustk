import React from 'react'
import PropTypes from 'prop-types'
import { TableBody } from '@mui/material'
import { networkStakeholderType } from '../../../types'
import { AGILE_EVALUATION_CATEGORY_DISPLAY_ORDER } from '../../../constants'
import AgileWaterfallRow from './AgileWaterfallRow'

function AgileWaterfallHead({
	networkStakeholders,
	agileEvaluations,
	updateAgileEvaluation,
}) {
	return (
		<TableBody>
			{
				AGILE_EVALUATION_CATEGORY_DISPLAY_ORDER.map(
					(evaluationCategory, index) => (
						<AgileWaterfallRow
							key={evaluationCategory}
							evaluationCategory={evaluationCategory}
							networkStakeholders={networkStakeholders}
							agileEvaluationsForEvaluationCategory={agileEvaluations[index]}
							updateAgileEvaluation={updateAgileEvaluation}
						/>
					),
				)
			}
		</TableBody>
	)
}

AgileWaterfallHead.propTypes = {
	networkStakeholders: PropTypes.arrayOf(networkStakeholderType).isRequired,
	agileEvaluations: PropTypes.arrayOf(
		PropTypes.arrayOf(PropTypes.string),
	).isRequired,
	updateAgileEvaluation: PropTypes.func.isRequired,
}

export default AgileWaterfallHead
