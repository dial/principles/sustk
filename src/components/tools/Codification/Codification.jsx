import React from 'react'
import {
	useRecoilValue,
	useSetRecoilState,
} from 'recoil'
import Link from '@docusaurus/Link'
import {
	TableContainer,
	Table,
	TableHead,
	TableBody,
	TableFooter,
	TableRow,
	TableCell,
} from '@mui/material'
import styles from '../Tools.modules.css'
import InnerTableAlertRow from '../InnerTableAlertRow'
import { getProductComponentIndex } from '../CoreModularHackable/utils'
import productComponentsAtom, { withCodificationFilters } from '../../../recoil/data/productComponents'
import CodificationEditorRow from './CodificationEditorRow'
import CodificationEditorFooter from './CodificationEditorFooter'

function Codification() {
	const productComponents = useRecoilValue(productComponentsAtom)
	const productComponentsWithoutBlanks = useRecoilValue(withCodificationFilters)
	const setProductComponents = useSetRecoilState(productComponentsAtom)

	const updateProductComponent = (productComponent) => {
		const updateIndex = getProductComponentIndex(productComponent, productComponents)
		const newProductComponents = [...productComponents]
		newProductComponents[updateIndex] = productComponent
		setProductComponents(newProductComponents)
	}

	const hasProductComponentsWithoutBlanks = productComponentsWithoutBlanks.length > 0

	// TODO: Build out Download PDF functionality (#129)
	// const showDownloadButton = (hasProductComponentsWithoutBlanks)
	const showDownloadButton = false

	return (
		<div className={styles.interactive}>
			<TableContainer>
				<Table aria-label="Codification Tool">
					<TableHead>
						<TableRow className={styles.header}>
							<TableCell component="th">
								ID
							</TableCell>
							<TableCell component="th">
								Component
							</TableCell>
							<TableCell
								component="th"
								width={85}
							>
								Current
							</TableCell>
							<TableCell
								component="th"
								width={85}
							>
								Target
							</TableCell>
							<TableCell component="th">
								By When?
							</TableCell>
							<TableCell component="th">
								Actions
							</TableCell>
						</TableRow>
					</TableHead>
					<TableBody>
						{hasProductComponentsWithoutBlanks
							? productComponentsWithoutBlanks.map((productComponent) => (
								<CodificationEditorRow
									key={productComponent.key}
									productComponent={productComponent}
									updateProductComponent={updateProductComponent}
								/>
							)) : (
								<InnerTableAlertRow colSpan={6}>
									Before you can codify your components, you have to first define them using the
									{' '}
									<Link to="./1.2b-core-modular-hackable">
										Core, Modular, or Hackable tool
									</Link>
									.
								</InnerTableAlertRow>
							)}
					</TableBody>
					<TableFooter>
						<TableRow>
							<TableCell colSpan={6}>
								<CodificationEditorFooter
									showDownloadButton={showDownloadButton}
								/>
							</TableCell>
						</TableRow>
					</TableFooter>
				</Table>
			</TableContainer>
			<p className="inline-credit">© Ian Gray</p>
		</div>
	)
}

export default Codification
