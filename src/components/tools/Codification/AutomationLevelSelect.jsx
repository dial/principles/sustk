import React from 'react'
import {
	Select,
	MenuItem,
} from '@mui/material'
import { PRODUCT_COMPONENT_AUTOMATION_LEVEL } from '../../../constants'

function getLabelForValue(value) {
	switch (value) {
	case PRODUCT_COMPONENT_AUTOMATION_LEVEL.ZERO:
		return '0'
	case PRODUCT_COMPONENT_AUTOMATION_LEVEL.ONE:
		return '1'
	case PRODUCT_COMPONENT_AUTOMATION_LEVEL.TWO:
		return '2'
	case PRODUCT_COMPONENT_AUTOMATION_LEVEL.THREE:
		return '3'
	case PRODUCT_COMPONENT_AUTOMATION_LEVEL.FOUR:
		return '4'
	case PRODUCT_COMPONENT_AUTOMATION_LEVEL.FIVE:
		return '5'
	case PRODUCT_COMPONENT_AUTOMATION_LEVEL.SIX:
		return '6'
	case PRODUCT_COMPONENT_AUTOMATION_LEVEL.SEVEN:
		return '7'
	default:
		return ''
	}
}

function AutomationLevelSelect(props) {
	/* eslint-disable react/jsx-props-no-spreading */
	// This is an intermediate component, so we really do want to just pass along all props
	// so that this component can be treated identically to the MUI Select component.
	return (
		<Select
			{...props}
			renderValue={getLabelForValue}
		>
			<MenuItem value={PRODUCT_COMPONENT_AUTOMATION_LEVEL.ZERO}>
				0 – Inside one person’s head
			</MenuItem>
			<MenuItem value={PRODUCT_COMPONENT_AUTOMATION_LEVEL.ONE}>
				1 – Inside several individuals’ heads
			</MenuItem>
			<MenuItem value={PRODUCT_COMPONENT_AUTOMATION_LEVEL.TWO}>
				2 – Can be shown
			</MenuItem>
			<MenuItem value={PRODUCT_COMPONENT_AUTOMATION_LEVEL.THREE}>
				3 – Can be shown and described verbally
			</MenuItem>
			<MenuItem value={PRODUCT_COMPONENT_AUTOMATION_LEVEL.FOUR}>
				4 – Is written down or drawn in a document
			</MenuItem>
			<MenuItem value={PRODUCT_COMPONENT_AUTOMATION_LEVEL.FIVE}>
				5 – Can be systematically replicated based on documentation
			</MenuItem>
			<MenuItem value={PRODUCT_COMPONENT_AUTOMATION_LEVEL.SIX}>
				6 – Is partially automated
			</MenuItem>
			<MenuItem value={PRODUCT_COMPONENT_AUTOMATION_LEVEL.SEVEN}>
				7 – Is fully automated
			</MenuItem>
		</Select>
	)
	/* eslint-enable react/jsx-props-no-spreading */
}

export default AutomationLevelSelect
