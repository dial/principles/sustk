import React from 'react'
import {
	Select,
	MenuItem,
} from '@mui/material'
import { SEGMENT_DESIRABIILTY } from '../../../constants'

function getLabelForValue(value) {
	switch (value) {
	case SEGMENT_DESIRABIILTY.FIVE:
		return '+5'
	case SEGMENT_DESIRABIILTY.FOUR:
		return '+4'
	case SEGMENT_DESIRABIILTY.THREE:
		return '+3'
	case SEGMENT_DESIRABIILTY.TWO:
		return '+2'
	case SEGMENT_DESIRABIILTY.ONE:
		return '+1'
	case SEGMENT_DESIRABIILTY.ZERO:
		return '0'
	case SEGMENT_DESIRABIILTY.NEGATIVE_ONE:
		return '-1'
	case SEGMENT_DESIRABIILTY.NEGATIVE_TWO:
		return '-2'
	case SEGMENT_DESIRABIILTY.NEGATIVE_THREE:
		return '-3'
	case SEGMENT_DESIRABIILTY.NEGATIVE_FOUR:
		return '-4'
	case SEGMENT_DESIRABIILTY.NEGATIVE_FIVE:
		return '-5'
	default:
		return ''
	}
}

function SegmentDesirabilitySelect(props) {
	/* eslint-disable react/jsx-props-no-spreading */
	// This is an intermediate component, so we really do want to just pass along all props
	// so that this component can be treated identically to the MUI Select component.
	return (
		<Select
			{...props}
			renderValue={getLabelForValue}
			fullWidth
		>
			<MenuItem value={SEGMENT_DESIRABIILTY.FIVE}>
				+5 (Very Desirable)
			</MenuItem>
			<MenuItem value={SEGMENT_DESIRABIILTY.FOUR}>
				+4
			</MenuItem>
			<MenuItem value={SEGMENT_DESIRABIILTY.THREE}>
				+3
			</MenuItem>
			<MenuItem value={SEGMENT_DESIRABIILTY.TWO}>
				+2
			</MenuItem>
			<MenuItem value={SEGMENT_DESIRABIILTY.ONE}>
				+1
			</MenuItem>
			<MenuItem value={SEGMENT_DESIRABIILTY.ZERO}>
				0 (Neutral)
			</MenuItem>
			<MenuItem value={SEGMENT_DESIRABIILTY.NEGATIVE_ONE}>
				-1
			</MenuItem>
			<MenuItem value={SEGMENT_DESIRABIILTY.NEGATIVE_TWO}>
				-2
			</MenuItem>
			<MenuItem value={SEGMENT_DESIRABIILTY.NEGATIVE_THREE}>
				-3
			</MenuItem>
			<MenuItem value={SEGMENT_DESIRABIILTY.NEGATIVE_FOUR}>
				-4
			</MenuItem>
			<MenuItem value={SEGMENT_DESIRABIILTY.NEGATIVE_FIVE}>
				-5 (Very Undesirable)
			</MenuItem>
		</Select>
	)
	/* eslint-enable react/jsx-props-no-spreading */
}

export default SegmentDesirabilitySelect
