import React, { useState, useEffect } from 'react'
import PropTypes from 'prop-types'
import { STICKY_MAX_CHAR_LENGTH } from '../../constants'
import stickyStyles from './Sticky.module.css'
import styles from './StickyEditField.module.css'

function StickyEditField({ value, onChange, segment }) {
	const [charsRemaining, setCharsRemaining] = useState(STICKY_MAX_CHAR_LENGTH)

	const getRemainingCharCount = (str) => STICKY_MAX_CHAR_LENGTH - str.length

	useEffect(() => {
		setCharsRemaining(getRemainingCharCount(value))
	}, [])

	const changeHandler = (e) => {
		const currValue = e.target.value
		setCharsRemaining(getRemainingCharCount(currValue))
		if (onChange) {
			onChange(e)
		}
	}

	return (
		<>
			{/* Wrap in div because textarea does not allow ::after for fold effect */}
			<div className={`
				${styles.textareaWrapper}
				${stickyStyles[`segment--${segment || 'none'}`]}
				${stickyStyles.fold}
			`}
			>
				<textarea
					className={styles.textarea}
					placeholder="Start typing here..."
					onChange={changeHandler}
					value={value}
				/>
			</div>
			<p
				className={`
					${styles.charCountText}
					${charsRemaining < 0 ? styles.charCountExceeded : ''}
				`}
			>
				{Math.abs(charsRemaining)}
				{' '}
				{Math.abs(charsRemaining) === 1 ? 'character' : 'characters'}
				{' '}
				{charsRemaining < 0 ? 'over recommended limit' : 'remaining'}
			</p>
		</>
	)
}

StickyEditField.propTypes = {
	value: PropTypes.string,
	onChange: PropTypes.func,
	segment: PropTypes.string,
}

StickyEditField.defaultProps = {
	value: '',
	onChange: null,
	segment: 'default',
}

export default StickyEditField
