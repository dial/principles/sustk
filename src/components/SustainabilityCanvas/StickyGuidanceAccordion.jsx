import * as React from 'react'
import PropTypes from 'prop-types'
import {
	Accordion,
	AccordionSummary,
	AccordionDetails,
} from '@mui/material'
import { BookOpenIcon } from '@heroicons/react/outline'

function StickyGuidanceAccordion(props) {
	const { header, children } = props

	return (
		<Accordion elevation={0}>
			<AccordionSummary
				aria-controls="panel1a-content"
				id="panel1a-header"
			>
				<h3>
					<BookOpenIcon className="icon" />
					{header}
				</h3>
			</AccordionSummary>
			<AccordionDetails>
				{children}
			</AccordionDetails>
		</Accordion>
	)
}

StickyGuidanceAccordion.propTypes = {
	header: PropTypes.string.isRequired,
	children: PropTypes.node.isRequired,
}

export default StickyGuidanceAccordion
