---
related_topics: ['revenue-streams','value-proposition','customer-relationships','key-partnerships','end-game']
---

import CostReductionModelCardDeck from '../../../src/components/tools/CostReductionModelExplorer/CostReductionModelCardDeck'
import CostReductionModelExplorerModal from '../../../src/components/tools/CostReductionModelExplorer/CostReductionModelExplorerModal'
import CostReductionModelExplorerUserDeck from '../../../src/components/tools/CostReductionModelExplorer/CostReductionModelExplorerUserDeck'

# Cost Reduction Models

Revenue is vital for the growth and survival of your organization and the maintenance and future release of your digital solution, but it is not the only factor. To evaluate your organization’s financial health, spending must also be considered.

When costs exceed revenue, your organization will have a negative net income, or a net loss. To avoid this situation and ensure the financial sustainability and impact of your digital solution, you should act to **reduce costs, maximize liquidity, and capture savings**.[^1]

:::tip Key Resources

- Video: [How Digital Technology Is Transforming Cost Management](https://knowledge.wharton.upenn.edu/article/cost-management-in-the-digital-age/)
- Document: [Exploring Greater Impact Through Strategic Partnerships](https://www.thepowerofpossibility.org/)
- Document: [Tips to Navigate Financial Crisis](https://nff.org/fundamental/tips-navigate-financial-crisis)
- Article: [Cost Reduction Tactics to Launch the Program](https://www.leanmap.com/consulting/cost-reduction/)

:::

Keeping an eye on costs can help organizations of all sizes expand their mission, any solution must be cost driven right from the start if it is to be successful and sustainable.[^2] Use the cost reduction strategy cards to stimulate ideas for how you might reduce costs in your business model. We recommend doing the following with your team:

1. Set aside 60 minutes.
2. Ask participants to read through the cards for about 10-15 minutes and then choose two or three cards that they think hold the possibility for reducing costs in the business model.
3. Discuss the cards that members of your team have chosen.
4. As a group, prioritize and choose which of the cost reduction strategies you would like to take forward and test.

Organizations should seek in-kind gifts or contributions, and identify models of service that may help in **reducing costs and realizing savings**.

## In-Kind Contributions

In-kind contributions are donations of goods, services, or time—as opposed to cash—that can be used to support an organization’s mission. Keep track of these voluntary contributions in a [chart of accounts](https://bench.co/blog/accounting/chart-of-accounts/).

<CostReductionModelCardDeck costReductionModelCategoryId="inkind" />

## Crowdsourcing Models

Crowdsourcing models harness the time, skills, and resources of large, disparate groups of people, often using online platforms. These contributors may be paid on a pay-per-task basis or may work as volunteers. This helps organizations reduce their overhead costs for traditional staff and foster opportunities for collaboration. Additionally, this model may offer organizations an easy way to design and develop new products, build new connections, and develop talent that may also assist with costs and long-term sustainability. Organizations can develop their own volunteer community or use a crowdsourcing platform.

<CostReductionModelCardDeck costReductionModelCategoryId="crowdsourcing" />

## Product Lifecycle and Channel Activities

Cost innovation strategies can be found in how your products or services are designed, sourced, and delivered.[^3] (See [Value Proposition](../value-proposition).)

<CostReductionModelCardDeck costReductionModelCategoryId="lifecycle" />

## Forging Partnerships

From supporting your organization by paying for your overhead costs to enhancing your products and services to increasing outreach efforts, partnerships can help organizations improve efficiency, impact, and sustainability. Defining what you want to get out of this collaborative relationship is essential.[^4] (See [Partnerships](../key-partnerships).)

<CostReductionModelCardDeck costReductionModelCategoryId="partnerships" />

<CostReductionModelExplorerModal />
<CostReductionModelExplorerUserDeck />

<p className="inline-credit">Created by Cristina Alves, Eliana Fram, and Ian Gray</p>

## Key Takeaways

1. Controlling costs is key to business model sustainability.

2. Opportunities to lower costs can be found within your organization and by leveraging partners.

3. Using a cost reduction strategy can enhance your organization’s potential value.

:::caution Complete the following in your [Business Model Sustainability Canvas](/canvas):

- Indicate which cost reduction strategies your organization will be using and their value.

:::

[^1]: Deloitte. [“Strategic Cost Transformation.”](https://www2.deloitte.com/content/dam/Deloitte/dk/Documents/Grabngo/STC_GnG_webinar_171219.pdf) (2019).
[^2]: MacKrell, L.l., Belton, A., Gottfredson, M., Fisher, J. (2017). [“Cutting Costs to Increase Impact.”](https://ssir.org/articles/entry/cutting_costs_to_increase_impact) Stanford Social Innovation Review.
[^3]: Ibid.
[^4]: Chung, E. [“How These 3 Nonprofit Partnerships Are Making An Impact.”](https://www.classy.org/blog/how-these-3-nonprofit-partnerships-are-making-an-impact/)
