---
slug: "."
sidebar_label: "Overview"
pagination_label: "Cost Structure Overview"
relevant_principles: ['understand-existing-ecosystem','design-for-scale','build-for-sustainability','be-data-driven','be-collaborative']
related_topics: ['revenue-streams','value-proposition','key-activities','key-resources','end-game']
---

import Link from '@docusaurus/Link'
import useBaseUrl from '@docusaurus/useBaseUrl'

# Cost Structure Overview

**Cost structure** is defined as all the costs and expenses your organization will incur while operating your business model.

**Cost and revenue are two sides of the same coin.** You will need revenue to sustain the ongoing expenses you incur while developing and maintaining your digital solutions, sustaining your teams, and scaling your impact. To be a sustainable organization, it is just as important to understand how funding is spent as how it is made.

<div className="admonition admonition-tip admonition-tip--pixelated alert alert--success">
	<div className="admonition-heading">
		<h5>Relevant Principles</h5>
	</div>
	<div className="admonition-content">
		<div className="relevant-principles">
			<Link
				to="https://digitalprinciples.org/principle/understand-the-existing-ecosystem/"
				className="relevant-principle"
			>
				<img
					src={useBaseUrl('/icons/principles/Icon_UnderstandExistingEcosystem.svg')}
					className="relevant-principle__icon"
				/>
				<span className="relevant-principle__label">
					Understand the Existing Ecosystem
				</span>
			</Link>
			<Link
				to="https://digitalprinciples.org/principle/design-for-scale/"
				className="relevant-principle"
			>
				<img
					src={useBaseUrl('/icons/principles/Icon_DesignForScale.svg')}
					className="relevant-principle__icon"
				/>
				<span className="relevant-principle__label">
					Design for Scale
				</span>
			</Link>
			<Link
				to="https://digitalprinciples.org/principle/build-for-sustainability/"
				className="relevant-principle"
			>
				<img
					src={useBaseUrl('/icons/principles/Icon_Sustainability.svg')}
					className="relevant-principle__icon"
				/>
				<span className="relevant-principle__label">
					Build for Sustainability
				</span>
			</Link>
			<Link
				to="https://digitalprinciples.org/principle/be-data-driven/"
				className="relevant-principle"
			>
				<img
					src={useBaseUrl('/icons/principles/Icon_DataDriven.svg')}
					className="relevant-principle__icon"
				/>
				<span className="relevant-principle__label">
					Be Data Driven
				</span>
			</Link>
			<Link
				to="https://digitalprinciples.org/principle/be-collaborative/"
				className="relevant-principle"
			>
				<img
					src={useBaseUrl('/icons/principles/Icon_Collaborative.svg')}
					className="relevant-principle__icon"
				/>
				<span className="relevant-principle__label">
					Be Collaborative
				</span>
			</Link>
		</div>
	</div>
</div>

:::tip Key Resources

- Document: [Glossary of Financial Terms](https://www.propelnonprofits.org/wp-content/uploads/2017/10/glossary.pdf)

:::

Financial sustainability is defined as the way in which an organization generates revenue from various sources to cover its costs and continue operating.[^1] By keeping your costs low, you reduce the level of revenue you will need to generate for operational sustainability.

For digital solutions in the humanitarian and development sectors, there are significant factors that constrain revenue and what costs can be covered from revenue streams such as grants. For many of these solutions, the [end game](../end-game) will be to transfer the operations of the digital product or service to a civil society organization or local government. These entities may have limited budget and scope for generating revenue for ongoing deployment and maintenance. So ensuring that your costs are kept low will be one of the most significant steps you can take to guarantee that your product or service is transferable and sustainable in the long term.

Managing costs is the job of all members of your team, so make sure they are familiar with the approaches you are taking and give them input into them.

To complete the cost structure building block of your Business Model Sustainability Canvas, you will need to focus on two main areas. [Section 9.1](./9.1-cost-management-and-cost-restrictions-in-the-aid-sector) will discuss cost management and cost restrictions, and [Section 9.2](./9.2-cost-reduction-models) will provide guidance on cost reduction strategies that will help ensure the long-term sustainability of your digital solution.

## Section 9.1: Managing Costs in the Aid Sector

This section provides guidance on the peculiarities of finance in the aid sector and the basics on how to manage costs.

**Key discussion areas:**

- Identify the different types of costs and discuss how to understand them when building a budget
- Understand your financial statements to assess your organization’s financial position and reflect on costs that may not provide value

## Section 9.2: Cost Reduction Strategies

This section explores cost reduction strategies (CRS) using cards similar to those used for revenue generation ideation in [Revenue Models in the Aid Sector](../revenue-streams/5.1-revenue-models-in-the-aid-sector).

** Key discussion areas:**

- Explore cost reduction strategies that may fall into these four types: in-kind contributions, crowdsourcing models, product lifecycle and channel activities, and forging partnerships
- Identify cost reduction ideas within each of these strategies that seem most appropriate for your organization and solution

## Key Takeaways

1. Understanding how funding works in the aid sector is critical to pursuing effective cost management practices, which will help digital solutions achieve long-term sustainability.

2. Trying to keep costs at their lowest is a significant step you can take to ensure that your product or service is transferable and sustainable in the long term.

3. Cost reduction strategies should be identified and employed with the aim of implementing them to increase the resilience and sustainability of your business model.

:::caution Complete the following in your [Business Model Sustainability Canvas](/canvas):

- Identify the key cost drivers in your business model.
- Indicate which cost reduction strategies your organization will be using and their value

:::

[^1]: Beyond Scale, Module 2 - Business Model, page 49.
