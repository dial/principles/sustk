---
slug: "."
sidebar_label: "Overview"
pagination_label: "End Game Overview"
relevant_principles: ['understand-existing-ecosystem','build-for-sustainability','use-open-standards','reuse-and-improve','be-collaborative']
related_topics: ['value-proposition','organizational-development']
---

import Link from '@docusaurus/Link'
import useBaseUrl from '@docusaurus/useBaseUrl'

# End Game Overview

An **end game** is defined as how a product or service is delivered when it is fully mature. It is particularly focused on the organization or networks that will be responsible for the ongoing delivery of the digital product or service.

For entrepreneurs in the private sector, the end game is often having your digital solution bought by a larger company. However, for digital solutions that are designed for social impact, you need to have an alternate vision of what the end game will be. Once the solution is mature and scaled to the extent you plan for, you must decide who will deliver it. Will it be your organization or a network of users? Or will it be completely handed over to a local or national government to run?

If you are aiming for true long-term sustainability, then you might want to hand over the ongoing running and support of your digital solution to a local entity (if you are not that local entity). Doing this requires significant thought and planning, so don’t expect to hand over the keys with little notice.

To complete the end game building block in your Business Model Sustainability Canvas, you will need to understand the most common end game options available for delivery of your digital solution once it has matured and scaled, as well as the key aspects to consider when planning for each end game.

<div className="admonition admonition-tip admonition-tip--pixelated alert alert--success">
	<div className="admonition-heading">
		<h5>Relevant Principles</h5>
	</div>
	<div className="admonition-content">
		<div className="relevant-principles">
			<Link
				to="https://digitalprinciples.org/principle/understand-the-existing-ecosystem/"
				className="relevant-principle"
			>
				<img
					src={useBaseUrl('/icons/principles/Icon_UnderstandExistingEcosystem.svg')}
					className="relevant-principle__icon"
				/>
				<span className="relevant-principle__label">
					Understand the Existing Ecosystem
				</span>
			</Link>
			<Link
				to="https://digitalprinciples.org/principle/build-for-sustainability/"
				className="relevant-principle"
			>
				<img
					src={useBaseUrl('/icons/principles/Icon_Sustainability.svg')}
					className="relevant-principle__icon"
				/>
				<span className="relevant-principle__label">
					Build for Sustainability
				</span>
			</Link>
			<Link
				to="https://digitalprinciples.org/principle/use-open-standards-open-data-open-source-and-open-innovation/"
				className="relevant-principle"
			>
				<img
					src={useBaseUrl('/icons/principles/Icon_OpenSource.svg')}
					className="relevant-principle__icon"
				/>
				<span className="relevant-principle__label">
					Use Open Standards, Open Data, Open Source, and Open Innovation
				</span>
			</Link>
			<Link
				to="https://digitalprinciples.org/principle/reuse-and-improve/"
				className="relevant-principle"
			>
				<img
					src={useBaseUrl('/icons/principles/Icon_ReuseImprove.svg')}
					className="relevant-principle__icon"
				/>
				<span className="relevant-principle__label">
					Reuse and Improve
				</span>
			</Link>
			<Link
				to="https://digitalprinciples.org/principle/be-collaborative/"
				className="relevant-principle"
			>
				<img
					src={useBaseUrl('/icons/principles/Icon_Collaborative.svg')}
					className="relevant-principle__icon"
				/>
				<span className="relevant-principle__label">
					Be Collaborative
				</span>
			</Link>
		</div>
	</div>
</div>

## Section 11.1 End Game Options

This section outlines the most common end game options, including:

1. Give away
2. Replication
3. Government adoption
4. Local organization adoption
5. Merger or acquisition
6. Mission achievement
7. Sustained service by your organization
8. Winding down

*(Adapted from [Guglev & Stern, SSIR](https://web.archive.org/web/20210316210126/https://www.philanthropy.org.au/images/site/misc/Tools__Resources/Publications/2015/Winter_2015_Whats_Your_Endgame.pdf).)*

It then provides the key factors to think about and plan for within each end game.

## Key Takeaways

1. Ensuring the long-term sustainability of the solution may mean that your organization hands it over to another organization, government department, or network.

2. You may still play a supporting role in the end game.

3. Preparing for the end game takes time.

:::caution Complete the following in your [Business Model Sustainability Canvas](/canvas):

- Outline your end game choice.

:::
